package client;

import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import BankApp.Bank;
import BankApp.BankHelper;

public class Client {

	public static void main(String[] args) {
		// Initialiser l'ORB
		org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args,null);
		try {
			// Récupérer le service de nommage
			org.omg.CORBA.Object objref = orb.resolve_initial_references("NameService");
			NamingContextExt refNamingContext = NamingContextExtHelper.narrow(objref);
			
			// Récupérer la référence du Servant
			String name = "Hello";
			
			// Convertir la reférence du Servant
			Bank bank = BankHelper.narrow(refNamingContext.resolve_str(name));
			
			// Utiliser l'objet
			bank.createAccount("Jeremy", 10000, "You'll never know");
			System.out.println("Tout est OK !");
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getMessage());
		}

	}

}
