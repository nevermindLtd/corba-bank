package server;

import BankApp.AccountPOA;

public class AccountServant extends AccountPOA{
	private String owner;
	private float balance;
	private String address;
	
	public AccountServant(String owner,float balance,String address) {
		this.owner=owner;
		this.balance=balance;
		this.address=address;
	}
	
	public AccountServant(String owner,String address) {
		this.owner=owner;
		this.balance=0;
		this.address=address;
	}

	@Override
	public String owner() {
		return this.owner;
	}

	@Override
	public float balance() {
		return this.balance;
	}

	@Override
	public String address() {
		return this.address;
	}

	@Override
	public void address(String newAddress) {
		this.address=newAddress;
		
	}

	@Override
	public void debit(float amount) {
		this.balance -= amount;
		
	}

	@Override
	public void credit(float amount) {
		this.balance += amount;
		
	}

}
