package server;

import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;
import BankApp.Account;
import BankApp.AccountHelper;
import BankApp.BankPOA;

public class BankServant extends BankPOA {
	private String company;

	@Override
	public String company() {
		return this.company;
	}

	@Override
	public Account createAccount(String owner, float balance, String address) {
		AccountServant accountServant = new AccountServant(owner, balance, address);
		org.omg.CORBA.Object refAccountServant = null;
		try {
			refAccountServant = _poa().servant_to_reference(accountServant);
		} catch (ServantNotActive | WrongPolicy e) {
			System.err.println("Servant Reference Failed");
			e.printStackTrace();
			System.exit(1);
		}
		Account account = AccountHelper.narrow(refAccountServant);
		return account;
	}

}
